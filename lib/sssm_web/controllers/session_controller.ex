defmodule SSSMWeb.SessionController do
  use SSSMWeb, :controller

  alias SSSM.Auth

  plug SSSMWeb.Plugs.AuthenticateUser when action in [:delete]

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => %{"username" => username, "password" => password}}) do
    case Auth.sign_in(username, password) do
      {:ok, user} ->
        conn
        |> put_session(:current_user_id, user.id)
        |> put_flash(:info, "You have successfully signed in!")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, _reason} ->
        conn
        |> put_flash(:error, "Invalid Username or Password")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> Auth.sign_out()
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
