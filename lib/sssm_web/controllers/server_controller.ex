defmodule SSSMWeb.ServerController do
  use SSSMWeb, :controller
  alias SSSM.Stats

  plug SSSMWeb.Plugs.AuthenticateUser

  def index(conn, _params) do
    servers = Stats.get_user_servers(conn.assigns.current_user.id)

    conn
    |> assign(:servers, servers)
    |> render("index.html")
  end

  def create(conn, params) do
    user_id = conn.assigns.current_user.id
    IO.inspect(user_id)

    case Stats.create_server(user_id) do
      {:ok, server} -> json(conn, %{token: server.token})
      _ -> json(conn, %{error: "Something wrong"})
    end
  end
end
