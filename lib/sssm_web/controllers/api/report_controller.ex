defmodule SSSMWeb.ReportController do
  use SSSMWeb, :controller
  alias SSSM.Stats

  def create(conn, params) do
    token = conn |> get_req_header("sssm-token")
    ip = to_string(:inet_parse.ntoa(conn.remote_ip))
    IO.inspect(params)

    with {:ok, token} <- extract_sssm_token(conn),
         {:ok, report} <- construct_stats_map(params, token, ip) do
      json(conn, params)
    else
      {:error, msg} -> json(conn, msg)
      {_, msg} -> json(conn, msg)
    end
  end

  defp extract_sssm_token(conn) do
    token = conn |> get_req_header("sssm-token") |> hd

    case token do
      [] -> {:error, "No sssm-token supplied"}
      x -> {:ok, x}
    end
  end

  defp construct_stats_map(params, token, ip) do
    params
    |> Map.put("token", token)
    |> Map.put("ip", ip)
    |> SSSM.Stats.insert_stats()
  end
end
