defmodule SSSMWeb.Router do
  use SSSMWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    # plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug SSSMWeb.Plugs.SetCurrentUser
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SSSMWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/sessions", SessionController, only: [:new, :create]
    delete "/sign_out", SessionController, :delete

    resources "/registrations", RegistrationController, only: [:new, :create]

    resources "/servers", ServerController, only: [:create, :index]
  end

  scope "/api", SSSMWeb do
    pipe_through :api

    post "/reports", ReportController, :create
  end

  # Other scopes may use custom stacks.
  # scope "/api", SSSMWeb do
  #   pipe_through :api
  # end
end
