defmodule SSSMWeb.ServerView do
  use SSSMWeb, :view
  alias SSSM.Stats
  def build_data(%Stats.Server{} = server) do
    case hd(server.reports) do
      [] -> nil
      x ->
        minutes = NaiveDateTime.diff(NaiveDateTime.utc_now(), x.inserted_at, :second) / 60 |> round

        %{hostname: x.hostname, ip: x.ip, last: minutes,
        image: "/images/distros/" <> distro_logo(x.distro), uptime: x.uptime,
        ram: ram_calc(x), disk: disk_calc(x) }
    end
  end

  def ram_calc(%{ram_used: ru, ram_total: rt})
    when ru > 0, do: (rt / ru) |> Float.round(2)
  def ram_calc(_), do: 0

  def disk_calc(%{disk_used: du, disk_total: dt})
    when du > 0, do: (dt / du) |> Float.round(2)
  def disk_calc(_), do: 0

  def distro_logo("Debian" <> _), do: "debian.png"
  def distro_logo("Ubuntu" <> _), do: "ubuntu.png"
  def distro_logo("CentOS" <> _), do: "centos.png"
  def distro_logo("Raspbian" <> _), do: "raspbian.png"
  def distro_logo("Fedora" <> _), do: "fedora.png"
  def distro_logo("Arch" <> _), do: "arch.png"
  def distro_logo("Mint" <> _), do: "mint.png"
  def distro_logo("Manjaro" <> _), do: "manjaro.png"
  def distro_logo("Antergos" <> _), do: "antergos.png"

end
