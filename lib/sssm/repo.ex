defmodule SSSM.Repo do
  use Ecto.Repo,
    otp_app: :sssm,
    adapter: Ecto.Adapters.Postgres
end
