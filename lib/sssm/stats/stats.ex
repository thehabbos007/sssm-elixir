defmodule SSSM.Stats do
  import Ecto.Query
  alias SSSM.Repo
  alias SSSM.Stats.Server
  alias SSSM.Stats.Report

  def insert_stats(params) do
    case get_server_by_token(params["token"]) do
      nil ->
        {:error, "Could not find server!"}

      server ->
        report_params = Map.put(params, "server_id", server.id)

        Report.changeset(%Report{}, report_params)
        |> Repo.insert()
    end
  end

  def get_server_by_token(token) do
    Repo.get_by(Server, token: token)
    |> IO.inspect()
  end

  def create_server(user_id) do
    server_params = %{token: random_string(16), user_id: user_id}

    Server.changeset(%Server{}, server_params)
    |> Repo.insert()
  end

  def random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64() |> binary_part(0, length)
  end

  def get_user_servers(user_id) do
    Repo.preload (Repo.all Server),
    reports: from(r in Report,
    distinct: r.server_id, order_by: [desc: r.updated_at])
  end

  def get_user_server_data(user_id) do
    Repo.all from(s in Server,
    where: s.user_id == ^user_id,
    preload: [:reports])
  end
end
