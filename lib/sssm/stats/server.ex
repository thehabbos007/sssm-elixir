defmodule SSSM.Stats.Server do
  use Ecto.Schema
  import Ecto.Changeset

  alias SSSM.Stats.Report

  schema "servers" do
    field :token, :string

    has_many :reports, Report
    belongs_to :user, SSSM.Auth.User

    timestamps()
  end

  @doc false
  def changeset(server, attrs) do
    IO.inspect(attrs)

    server
    |> cast(attrs, [:token, :user_id])
    |> unique_constraint(:token)
    |> validate_required([:token, :user_id])
    |> foreign_key_constraint(:user_id)
  end
end
