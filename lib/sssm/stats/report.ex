defmodule SSSM.Stats.Report do
  use Ecto.Schema
  import Ecto.Changeset

  alias SSSM.Stats.Server

  @required_fields ~w(uptime hostname distro ip ram_total ram_used disk_total disk_used server_id)a

  schema "reports" do
    field :uptime, :float
    field :hostname, :string
    field :distro, :string
    field :ip, :string
    field :ram_total, :float
    field :ram_used, :float
    field :disk_total, :float
    field :disk_used, :float
    belongs_to :server, Server

    timestamps()
  end

  @doc false
  def changeset(server, attrs) do
    IO.inspect(attrs)

    server
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:server_id)
  end
end
