# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :sssm,
  namespace: SSSM,
  ecto_repos: [SSSM.Repo]

# Configures the endpoint
config :sssm, SSSMWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FovUCioJiYVRixoUNrbinWPiQgdNEG/G5zX+2zfrm42kaqeU1VSMnlqRIcRxERuf",
  render_errors: [view: SSSMWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SSSM.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
