defmodule SSSM.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :string, null: false
      add :encrypted_password, :string, null: false

      timestamps()
    end

    create unique_index(:users, [:username])

    create table(:servers) do
      add :token, :string, null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:servers, [:token])
    create index(:servers, [:user_id])

    create table(:reports) do
      add :uptime, :float, null: false
      add :hostname, :string, null: false
      add :distro, :string, null: false
      add :ip, :string, null: false
      add :ram_total, :float, null: false
      add :ram_used, :float, null: false
      add :disk_total, :float, null: false
      add :disk_used, :float, null: false
      add :server_id, references(:servers, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
